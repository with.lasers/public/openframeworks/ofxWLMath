# WithLasers::Math::Easings

A collection of easing functions, based on these [functions][1]

## Usage

There are couple of different ways the functions can be called

1. Directly - each function is available in the form `ease/In/Out/Function` and expect a normalised input for x (i.e. 0 -> 1)
2. Generically by function - e.g. `sine`, `quart` etc - as well as the normalised input, you need to pass a mode `IN|OUT|IN_OUT`
3. Generically using function `ease` as for 2, except function name is passed using the values in enum Function

### Utility function

You can get a text desctiption of an easing curve (e.g. ease in out-cubic) using the function `getLabel()`


## Examples

``` cpp
#include "easings.h

using namespace WithLasers;

  //1. Direct call

  float y = Math::Easings::easeInQuart(0.3f);

  float y = Math::Easings::easeOutSine(0.1f);

  float y = Math::Easings::easeInOutCubic(0.7f);

  //2. Generic by function

  float y = Math::Easings::quart(Math::Easings::IN, 0.3f);

  float y = Math::Easings::sine(Math::Easings::OUT, 0.1f);

  float y = Math::easings::Cubic(Math::easings::IN_OUT, 0.7f);

  //3. Generic

  float y = Math::Easings::ease(Math::Easings::QUART, Math::Easings::IN, 0.3f);

  float y = Math::Easings::ease(Math::Easings::SINE, Math::Easings::OUT, 0.1f);

  float y = Math::easings::ease(Math::Easings::CUBE, Math::Easings::OUT, 0.7f);

  //Utility for text description

  string label = Math::Easings::getLabel(Math::Easings::QUINT, Math::Easings::IN_OUT);


```














[1]:https://easings.net
