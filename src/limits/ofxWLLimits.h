/*
  ofxWLLimits - useful functions aound limits
  2021/2022 with-lasers.studio

  Global header which includes all libraries used for limits, they can also
  be referenced individually if required
*/

#pragma once

#include "ofxWLBounds.h"            //Bounds handling in 1,2 & 3 dimensions