/*
  ofxWLBounds - store and update bounds in 1,2 and 3 dimensions
  2021/2022 with-lasers.studio

  Represent minimum and maximum amounts, for example bounds of a 2D / 3D object, the Bounds(2/3)
  structure gives the minimum and maximum values. These are updated, by using the cooresponding
  test function testBounds which will update the minMax value and also return true if the bounds changed

  TODO:

  1) would be useful to have a test function which doesn't expand the bounds

*/

#pragma once

//Open Frameworks Includes
#include "ofMain.h"

//Add on Includes

//Local Includes


namespace ofxWithLasers {
  
  namespace Math {
    
    namespace Limits {

      //Structures for minmax data up to 3 dimensions
      struct Bounds {
        float min = 1000000;
        float max = 0;
      };
      
      struct Bounds2 {
        Bounds x;
        Bounds y;
      };
      
      struct Bounds3 {
        Bounds x;
        Bounds y;
        Bounds z;
      };

      //Some useful predefined bounds objects

      inline const Bounds PI_BOUNDS = {
        .min = 0.0f,
        .max = PI
      };

      inline const Bounds TWO_PI_BOUNDS = {
        .min = 0.0f,
        .max = TWO_PI
      };

      //Within functions, these test if a value is inside the bounds and
      //returns true of false based on that check
      inline bool within(float x, Bounds b) {
        return (x >= b.min && x <= b.max);
      }

      inline bool within(float x, float y, Bounds2 b) {
        return within(x, b.x) && within(y, b.y);
      }

      inline bool within(glm::vec2 p, Bounds2 b) {
        return within(p.x, p.y, b);
      }

      inline bool within(float x, float y, float z, Bounds3 b) {
        return within(x, b.x) && within(y, b.y) && within(z, b.z);
      }

      inline bool within(glm::vec3 p, Bounds3 b) {
        return within(p.x, p.y, p.z, b);
      }

      //Outside functions, these test if a value is outside the bounds and
      //returns true of false based on that check
      inline bool outside(float x, Bounds b) {
        return (x < b.min || x > b.max);
      }

      inline bool outside(float x, float y, Bounds2 b) {
        return outside(x, b.x) && outside(y, b.y);
      }

      inline bool outside(glm::vec2 p, Bounds2 b) {
        return outside(p.x, p.y, b);
      }

      inline bool outside(float x, float y, float z, Bounds3 b) {
        return outside(x, b.x) && outside(y, b.y) && outside(z, b.z);
      }

      inline bool outside(glm::vec3 p, Bounds3 b) {
        return outside(p.x, p.y, p.z, b);
      }


      //Out of bounds functions, these handle what should happen if a value os out of bounds
      enum OutOfBounds {
        OOB_CLAMP,  
        OOB_WRAP,
        OOB_REFLECT,
        OOB_ALLOW
      };

      inline float clamp(float v, Bounds b) {

        return ofClamp(v, b.min, b.max);

      }

      inline float wrap(float v, Bounds b) {

        float v_ = v;

          if (v > b.max) {
            v_ = v - b.max;
          }
          if (v < b.min) {
            v_ = b.max - v;
          }

          return v_;

      }

      inline float reflect(float v, Bounds b) {

        float v_ = v;

        if (v > b.max) {
          v_ = (2 * b.max) - v;
        }
        if (v < b.min) {
          v_ = b.min - v;
        }

        return v_;

      }

      inline bool oob(float& x, Bounds b, OutOfBounds action = OOB_CLAMP) {
        bool r = outside(x, b);
        if (r) {
          switch (action) {
            case OOB_CLAMP:
              x = clamp(x, b);
              break;
            case OOB_WRAP:
              x = wrap(x, b);
              break;
            case OOB_REFLECT:
              x = reflect(x, b);
              break;
            case OOB_ALLOW:
              break;
          }
        }
        return r;
      }

      inline bool oob(float& x, float& y, Bounds2 b, OutOfBounds action = OOB_CLAMP) {
        bool tx = oob(x, b.x, action);
        bool ty = oob(y, b.y, action); 
        return tx || ty;
      }

      inline bool oob(glm::vec2 p, Bounds2 b, OutOfBounds action = OOB_CLAMP) {
        return oob(p.x, p.y, b, action);
      }

      inline bool oob(float& x, float& y, float& z, Bounds3 b, OutOfBounds action = OOB_CLAMP) {
        bool tx = oob(x, b.x, action);
        bool ty = oob(y, b.y, action); 
        bool tz = oob(y, b.z, action); 
        return tx || ty || tz;
      }

      inline bool oob(glm::vec3 p, Bounds3 b, OutOfBounds action = OOB_CLAMP) {
        return oob(p.x, p.y, p.z, b, action);
      }

      //Update functions, these test if a value lies within the bounds
      //and if outside they extend the bounds, and return true
      inline bool update(float n, Bounds& m) {
        bool test = false;
        //test and update minmax values passed in by reference
        if (n > m.max) {
          m.max = n;
          test = true;
        } else if (n < m.min) {
          m.min = n;
          test = true;
        }
        return test;
      }

      inline bool update(float x, float y, Bounds2 &b) {
        bool t1 = update(x, b.x);
        bool t2 = update(y, b.y);
        return t1 || t2;
      }

      inline bool update(glm::vec2 v, Bounds2 &b) {
        return update(v.x, v.y, b);
      }
      
      inline  bool update(float x, float y, float z, Bounds3 &b) {
        bool t1 = update(x, b.x);
        bool t2 = update(y, b.y);
        bool t3 = update(z, b.z);
        return t1 || t2 || t3;
      }

      inline bool update(glm::vec3 v, Bounds3 &b) {
        return update(v.x, v.y, v.z, b);
      }
    }
  }
}
