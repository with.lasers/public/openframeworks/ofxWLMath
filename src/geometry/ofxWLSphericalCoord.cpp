#include "ofxWLSphericalCoord.h"

using namespace ofxWithLasers::Math;

//Setters
void Spherical::set(Coords c) {

  _spherical = c;
  _updateCartesian();

}

void Spherical::set(float phi, float rho) {

  _spherical.phi = phi;
  _spherical.theta = PI;
  _spherical.rho = rho;
  _updateCartesian();

}

void Spherical::set(glm::vec2 p) {

  set(glm::vec3(p.x, p.y, 0.0f));

}

void Spherical::set(glm::vec3 p) {

  _cartesian = p;
  _updateSpherical();

}

void Spherical::setXY(float x, float y) {

  setXYZ(x, y, 0.0f);

}

void Spherical::setXYZ(float x, float y, float z) {

  _cartesian.x = x;
  _cartesian.y = y;
  _cartesian.z = z;
  _updateSpherical();

}

//Getters
Spherical::Coords Spherical::spherical() {
  return _spherical;
}

glm::vec3 Spherical::cartesian() {
  return _cartesian;
}

//Adders

void Spherical::add(Coords d) {

  _spherical += d;
  _updateCartesian();

}

void Spherical::add(float dPhi, float dRho) {

  add(dPhi, 0.0f, dRho);

}

void Spherical::add(float dPhi, float dTheta, float dRho) {

  _spherical.phi += dPhi;
  _spherical.theta += dTheta;
  _spherical.rho += dRho;
  _updateCartesian();

}


void Spherical::addPhi(float dPhi) {

  _spherical.phi += dPhi;
  _updateCartesian();

}

void Spherical::addTheta(float dTheta) {

  _spherical.theta += dTheta;
  _updateCartesian();

}

void Spherical::addRho(float dRho) {

  _spherical.rho += dRho;
  _updateCartesian();

}

void Spherical::add(glm::vec2 dP) {

  add(glm::vec3(dP.x, dP.y, 0.0f));

}

void Spherical::add(glm::vec3 dP) {

  _cartesian += dP;
  _updateSpherical();

}


void Spherical::addXY(float dX, float dY) {

  addXYZ(dX, dY, 0.0f);

}

void Spherical::addXYZ(float dX, float dY, float dZ) {

  _cartesian.x += dX;
  _cartesian.y += dY;
  _cartesian.z += dZ;
  _updateSpherical();

}


void Spherical::addX(float dX) {

  _cartesian.x += dX;
  _updateSpherical();

}

void Spherical::addY(float dY) {

  _cartesian.y += dY;
  _updateSpherical();
  
}

void Spherical::addZ(float dZ) {

  _cartesian.z += dZ;
  _updateSpherical();
  
}


void Spherical::_updateCartesian() {

  //Caled when the spherical coordinates have chnaged, so we need new cartesian values
  _cartesian.x = _spherical.rho * cosf(_spherical.phi) * sinf(_spherical.theta);
  _cartesian.y = _spherical.rho * sinf(_spherical.theta) * sinf(_spherical.theta);
  _cartesian.z = _spherical.rho * cosf(_spherical.theta);

  cout << ofGetFrameNum() << " updated carteisan to " << _cartesian << " from rho: " << _spherical.rho << " theta: " << _spherical.theta << " phi: " << _spherical.phi << endl; 

}

void Spherical::_updateSpherical() {

  //Caled when the cartesian coordinates have chnaged, so we need new spherical values
  
  _spherical.rho = sqrt(_cartesian.x * _cartesian.x + _cartesian.y * _cartesian.y + _cartesian.z * _cartesian.z);
  _spherical.theta = acosf(_cartesian.z / _spherical.rho);
  _spherical.phi = atan2f(_cartesian.y, _cartesian.z);

}