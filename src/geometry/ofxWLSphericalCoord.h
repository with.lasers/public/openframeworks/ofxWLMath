/*
  ofxWLSphericalCoord - useful math functions for open frameworks
  2021/2022 with-lasers.studio

  Class to store and calculate spherical(polar) coordinates, values can be set by either
  cartesian or spherical values, they can be added (subtraction coming), and values can 
  be got either by cartesian or polar amouts

*/

#pragma once

//Open Frameworks Includes
#include "ofMain.h"


//Add on Includes

//Local Includes
#include "ofxWLLimits.h"



namespace ofxWithLasers {
  
  namespace Math {
    
    class Spherical {

      public:

        struct Coords {
          float phi = 0.0f;
          float theta = PI;
          float rho = 0.0f;

          

          inline Coords& operator += (const Coords& d) {

            if (this != &d) {
              this->phi += d.phi;
              Limits::oob(this->phi, Limits::TWO_PI_BOUNDS, Limits::OOB_WRAP);

              this->theta += d.theta;
              Limits::oob(this->theta, Limits::PI_BOUNDS, Limits::OOB_WRAP);

              this->rho += d.rho;
            }
            return *this;
          }

          inline const Coords operator + (const Coords& d) {
            Coords r = *this;
            r += d;
            return r;
          }

          inline bool operator == (Coords& d) {
            return d.phi == phi && d.rho == rho && d.theta == theta;
          }
          
        };
        
        //Two dimensional where z is always assumed 
        
        //Setters
        void set(Coords c);

        void set(float phi, float rho);  
        void set(float phi, float theta, float rho);

        void set(glm::vec2 p);
        void set(glm::vec3 p);

        void setXY(float x, float y);
        void setXYZ(float x, float y, float z);

        //Getters
        Coords spherical();
        glm::vec3 cartesian();

        //Operators
        void add(Coords d);
        
        void add(float dPhi, float dRho);
        void add(float dPhi, float theta, float dRho);
        
        void addPhi(float dPhi);
        void addTheta(float dTheta);
        void addRho(float dRho);

        void add(glm::vec2 p);
        void add(glm::vec3 p);

        void addXY(float x, float y);
        void addXYZ(float x, float y, float z);

        void addX(float x);
        void addY(float y);
        void addZ(float z);

       

      private:

        Coords _spherical;
        glm::vec3 _cartesian;

        void _updateCartesian();
        void _updateSpherical();



    };

  }

}