/*
  ofxWLMath - useful math functions for open frameworks
  2021/2022 with-lasers.studio

  Global header which includes all libraries used for geometry, they can also
  be referenced individually if required
*/

#pragma once

//Geometry
#include "ofxWLMathSpherical.h"           //Spherical / Polar coordinates and conversion
#include "ofxWLPointsLinesPlanes.h"       //Useful functions for points, lines and planes