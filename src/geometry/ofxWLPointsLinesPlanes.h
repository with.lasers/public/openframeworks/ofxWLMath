/*
  ofxWLMath - useful math functions for open frameworks
  2021/2022 with-lasers.studio

  Useful functions for points, lines amd planes - for example angle between lines, 
  distance to line etc

  Function catalog
  float angleBetweenLines(glm::vec2 p1, glm::vec2 o, glm::vec2 p2)
  float distanceToLine(glm::vec2 p, Line l)

*/

#include "ofMain.h"

namespace ofxWithLasers {
  
  namespace Math {
    
    namespace Geometry {
      
      struct Line {
        glm::vec2 p1;
        glm::vec2 p2;
      };
      
      inline float angleBetweenLines(glm::vec2 p1, glm::vec2 o, glm::vec2 p2) {
        
        //
        //https://stackoverflow.com/questions/31064234/find-the-angle-between-two-vectors-from-an-arbitrary-origin
        
        /*
         
                      p2
                    /
                   o
                    \
                     p1
         
         */
        
        glm::vec2 a = normalize(p1 - o);
        glm::vec2 b = normalize(p2 - o);
        return acos(glm::dot(a,b));
        
      };
      
      inline float distanceToLine(glm::vec2 p, Line l) {
        
        //TODO: Can we tempplate this to create a glm::vec2 and glm::vec3 version
        
        //Paul Bourke - Points, Lines &  Planes - http://paulbourke.net/geometry/pointlineplane/
        //Minimum Distance between a Point and a Line - 2D limitiation - should scale to 3 with glm::vec3…
        /*
                    l.p2
                   /
                  /
                 i
                / \ d
               /   \
              /     p3
            l.p1
         */
        float u = (((p.x - l.p1.x) * (l.p2.x - l.p1.x)) + ((p.y - l.p1.y) * (l.p2.y - l.p1.y))) / ( glm::length2(l.p2 - l.p1));
        if (u == 0.0f) {
          return -1.0f;
        } else {
          glm::vec2 i = glm::vec2(l.p1.x + (u * (l.p2.x - l.p1.x)), l.p1.y + (u * (l.p2.y - l.p1.y)));
          return glm::distance(p, i);
        }
      };
      
      
    }
    
  }
  
  
  
}
