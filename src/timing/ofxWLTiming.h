/*
  ofxWLTiming - useful functions around timings
  2021/2022 with-lasers.studio

  Global header which includes all libraries used for limits, they can also
  be referenced individually if required
*/

#pragma once

#include "ofxWLEasings.h"            //Various easing functions for remapping x/y values
#include "ofxWLDeltaT.h"             //Functions for working with time deltas

