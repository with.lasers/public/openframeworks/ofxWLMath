/*
  ofxWLEasings - collection of easing functions, with options for ease in, ease out, and both
  2021/2022 with-lasers.studio

*/

#pragma once

#include "ofMain.h"

namespace ofxWithLasers {

  namespace Math {

    namespace Easings {

      enum Function { LINEAR, SINE, QUAD, CUBE, QUART, QUINT, EXP, CIRC };

      enum Mode { IN, OUT, IN_OUT };

      struct Easing {
        Function f;
        Mode m;
      };

      inline const vector<string> functionNames = {"linear",  "sine",    "quadratic",   "cubic",
                                                   "quartic", "quintic", "exponential", "circular"};

      inline const vector<string> modeNames = {"out", "in", "in/out"};

      inline float easeInPolynomial(float x, int p) {
        return pow(x, p);
      }

      inline float easeOutPolynomial(float x, int p) {
        return 1.0f - pow(1.0f - x, p);
      }

      inline float easeInOutPolynomial(float x, int p) {
        float y;
        if (x < 0.5) {
          y = (pow(2, p - 1) * pow(x, p)) / 2.0f;
        } else {
          y = 1.0f - pow(-2.0f * x + 2.0f, p) / 2.0f;
        }
        return y;
      }

      inline float linear(float x) {
        return x;
      }

      inline float linear(float x, float b) {
        return x * b;
      }

      inline float linear(float x, float a, float b) {
        return a + (x * b);
      }

      inline float easeInSine(float x) {
        return 1.0f - cos((x * PI) / 2.0f);
      }

      inline float easeOutSine(float x) {
        return sin((x * PI) / 2.0f);
      }

      inline float easeInOutSine(float x) {
        return -(cos(PI * x) - 1.0f) / 2.0f;
      }

      inline float sine(Mode m, float x) {
        float y;
        switch (m) {
        case IN:
          y = easeInSine(x);
          break;
        case OUT:
          y = easeOutSine(x);
          break;
        case IN_OUT:
          y = easeInOutSine(x);
          break;
        }
        return y;
      }

      inline float easeInQuad(float x) {
        return easeInPolynomial(x, 2);
      }

      inline float easeOutQuad(float x) {
        return easeOutPolynomial(x, 2);
      }

      inline float easeInOutQuad(float x) {
        return easeInOutPolynomial(x, 2);
      }

      inline float quad(Mode m, float x) {
        float y;
        switch (m) {
        case IN:
          y = easeInQuad(x);
          break;
        case OUT:
          y = easeOutQuad(x);
          break;
        case IN_OUT:
          y = easeInOutQuad(x);
          break;
        }
        return y;
      }

      inline float easeInCubic(float x) {
        return easeInPolynomial(x, 3);
      }

      inline float easeOutCubic(float x) {
        return easeOutPolynomial(x, 3);
      }

      inline float easeInOutCubic(float x) {
        return easeInOutPolynomial(x, 3);
      }

      inline float cubic(Mode m, float x) {
        float y;
        switch (m) {
        case IN:
          y = easeInCubic(x);
          break;
        case OUT:
          y = easeOutCubic(x);
          break;
        case IN_OUT:
          y = easeInOutCubic(x);
          break;
        }
        return y;
      }

      inline float easeInQuart(float x) {
        return easeInPolynomial(x, 4);
      }

      inline float easeOutQuart(float x) {
        return easeOutPolynomial(x, 4);
      }

      inline float easeInOutQuart(float x) {
        return easeInOutPolynomial(x, 4);
      }

      inline float quart(Mode m, float x) {
        float y;
        switch (m) {
        case IN:
          y = easeInQuart(x);
          break;
        case OUT:
          y = easeOutQuart(x);
          break;
        case IN_OUT:
          y = easeInOutQuart(x);
          break;
        }
        return y;
      }

      inline float easeInQuint(float x) {
        return easeInPolynomial(x, 5);
      }

      inline float easeOutQuint(float x) {
        return easeOutPolynomial(x, 5);
      }

      inline float easeInOutQuint(float x) {
        return easeInOutPolynomial(x, 5);
      }

      inline float quint(Mode m, float x) {
        float y;
        switch (m) {
        case IN:
          y = easeInQuint(x);
          break;
        case OUT:
          y = easeOutQuint(x);
          break;
        case IN_OUT:
          y = easeInOutQuint(x);
          break;
        }
        return y;
      }

      inline float easeInExp(float x) {
        return x = (x == 0.0f) ? 0.0f : pow(2, (x - 10.0f) * 10.0f);
      }

      inline float easeOutExp(float x) {
        return x = (x == 1.0f) ? 1.0f : 1 - pow(2, x * -10.0f);
      }

      inline float easeInOutExp(float x) {

        float y;

        if (x == 0.0f || x == 1.0f) {
          y = x;
        } else if (x < 0.5f) {
          y = pow(2, 20.0f * x - 10.0f) / 2.0f;
        } else {
          y = (2.0f - pow(2, -20 * x + 10)) / 2.0f;
        }

        return y;
      }

      inline float exp(Mode m, float x) {

        float y;

        switch (m) {
        case IN:
          y = easeInExp(x);
          break;
        case OUT:
          y = easeOutExp(x);
          break;
        case IN_OUT:
          y = easeInOutExp(x);
          break;
        }

        return y;
      }

      inline float easeInCirc(float x) {
        return 1.0f - sqrt(1 - pow(x, 2));
      }

      inline float easeOutCirc(float x) {
        return sqrt(1.0f - pow(x - 1.0f, 2));
      }

      inline float easeInOutCirc(float x) {
        float y;
        if (x < 0.5f) {
          y = (1.0f - sqrt(1.0f - pow(2.0f * x, 2))) / 2.0f;
        } else {
          y = (sqrt(1.0f - pow(2.0f - (2.0f * x), 2)) + 1.0f) / 2.0f;
        }
        return y;
      }

      inline float circ(Mode m, float x) {
        float y;
        switch (m) {
        case IN:
          y = easeInCirc(x);
          break;
        case OUT:
          y = easeOutCirc(x);
          break;
        case IN_OUT:
          y = easeInOutCirc(x);
          break;
        }
        return y;
      }

      inline float ease(Function f, Mode m, float x, float a = 0.0f, float b = 1.0f) {
        float y;
        x = abs(x);
        switch (f) {
        case LINEAR:
          y = linear(x, a, b);
          break;
        case SINE:
          y = sine(m, x);
          break;
        case QUAD:
          y = quad(m, x);
          break;
        case CUBE:
          y = cubic(m, x);
          break;
        case QUART:
          y = quart(m, x);
          break;
        case QUINT:
          y = quint(m, x);
          break;
        case EXP:
          y = exp(m, x);
          break;
        case CIRC:
          y = circ(m, x);
          break;
        }
        return y;
      }

      inline float ease(Easing e, float x, float a = 0.0f, float b = 1.0f) {
        return ease(e.f, e.m, x, a, b);
      }

      inline string getLabel(Function f, Mode m) {

        string l;

        if (f == LINEAR) {
          l = functionNames[static_cast<Function>(f)];
        } else {
          l = functionNames[static_cast<Function>(f)] + "-" + modeNames[static_cast<Mode>(m)];
        }

        return l;
      }
    }
  }
}