/*
  ofxWLdTime - working with timing changes, e.g. frames etc
  2021/2022 with-lasers.studio

*/

#pragma once

#include "ofMain.h"

namespace ofxWithLasers {

  namespace Math {

    namespace Timing {

      enum Resolution { MILLI, MICRO };

      inline float calcFrameTime(float frameRate = 60.0f, Resolution r = MILLI) {
        // Calculate time per frame based on the resolution - note we pass in
        // a frame rate, rather than use ofGetFrameRate() as this returns a
        // current average not the desired frame rate - if you use frameRate
        // based calculation then do somsrehign like const int FRAME_RATE = 30;
        // ofSetFrameRate(FRAME_RATE);
        // float dT = Timing::calcFrameTime(FRAME_RATE);

        // Note this should be useful for to disk rendering, where you can set
        // everything up with the correct time frame per frame, irrespective of
        // that the app is doing
        return (r == MILLI) ? (1000 / frameRate) : (1000000 / frameRate);
      }

      class DeltaT {

      public:
        typedef unique_ptr<DeltaT> Ptr;

        inline DeltaT(Resolution r) {
          _resolution = r;
          _lastTS = _getTS();
        }

        inline float get() {

          uint64_t thisTS = _getTS();
          float _dT = thisTS - _lastTS;
          _lastTS = thisTS;
          return _dT;
        }

      private:
        Resolution _resolution;
        uint64_t _lastTS;

        inline uint64_t _getTS() {
          return (_resolution == MILLI) ? ofGetElapsedTimeMillis() : ofGetElapsedTimeMicros();
        }
      };

    }

  }
}
