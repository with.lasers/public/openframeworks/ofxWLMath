#include "ofxWLCoeffcients.h"

using namespace ofxWithLasers::Math;

//Linear Coeffcients
Coeffcient::Coeffcient(float v, float dV) {

  _v = v;
  _dV = dV;
  _action = Limits::OOB_ALLOW;
  _kind = LINEAR;

}

Coeffcient::Coeffcient(float v, float dV, Limits::Bounds b, Limits::OutOfBounds a) {
  
  _v = v;
  _dV = dV;
  _bounds = b;
  _action = a;
  _kind = LINEAR;

}

//Recursive Coeffcients
Coeffcient::Coeffcient(float v, Ptr dV) {

  _v = v;
  _dVC = dV;
  _action = Limits::OOB_ALLOW;
  _kind = RECURSIVE;

}

Coeffcient::Coeffcient(float v, Ptr dV, Limits::Bounds b, Limits::OutOfBounds a) {
  
  _v = v;
  _dVC = dV;
  _bounds = b;
  _action = a;
  _kind = RECURSIVE;

}

//Eased Coeffcients
Coeffcient::Coeffcient(float n, float scale, float dV, Limits::OutOfBounds a, Easings::Easing e) {

  _n = ofClamp(n, 0.0f, 1.0f);
  _s = scale;
  _dV = dV;
  _bounds.min = 0.0f;
  _bounds.max = 1.0f;
  _action = a == Limits::OOB_ALLOW ? Limits::OOB_CLAMP : a;
  _easing = e;
  _kind = EASED;


}

//Setters
void Coeffcient::setValue(float v) {
  _v = v;
}

void Coeffcient::setDelta(float dV) {
  if (_kind == RECURSIVE) {
    _dVC->setDelta(dV);
  } else {
    _dV = dV;
  }
}

void Coeffcient::setDelta(Ptr v) {
  _dVC = v;
  _kind = RECURSIVE;
}


//Getters
float Coeffcient::value() {
  return _v;
}

float Coeffcient::dValue() {

  float r = 0.0f;

  switch (_kind) {
    case LINEAR:
    case EASED:
      r = _dV;
      break;
    case RECURSIVE:
      r = _dVC->dValue();
      break;
  }

  return r;

}

//Update
float Coeffcient::update(float dT) {

  float r;

  // cout << "updating coeffcient was " <<  _v;

  switch (_kind) {
    case LINEAR:
      r = _updateLinear(dT);
      break;
    case RECURSIVE:
      r = _updateRecursive(dT);
      break;
    case EASED:
      r = _updateEased(dT);
      break;
  }

  // cout << " now " << r << endl;

  return r;

}

float Coeffcient::_updateLinear(float dT) {

  _v += _dV * dT;
  Limits::oob(_v, _bounds, _action);
  return _v;

}

float Coeffcient::_updateRecursive(float dT) {

  _v += _dVC->update(dT);
  Limits::oob(_v, _bounds, _action);
  return _v;

}

float Coeffcient::_updateEased(float dT) {

  _n += _dV * dT;
  Limits::oob(_n, _bounds, _action);
  _v = Easings::ease(_easing, _n) * _s;
  return _v;

}

//Coeffcients collection

Coeffcients::Coeffcients(bool autoUpdate, TimeBase base, Timing::Resolution res, float frameRate) {

  //If set to autoupdate (which is the default) then we will call
  //the updates of the coeffcients automitically
  if (autoUpdate) {
    _base = base;
    
    if (_base == FRAME_COUNT) {
      _frame_dT = Timing::calcFrameTime(frameRate, res);
    } else {
      _dT = make_unique<Timing::DeltaT>(res);
    }

    ofAddListener(ofEvents().update, this, &Coeffcients::autoUpdate, OF_EVENT_ORDER_BEFORE_APP);

  }

}

void Coeffcients::set(Alpha id, Coeffcient::Ptr c) {

  if (_coeffcients.count(id) > 0) {
    ofLogNotice("ofxWLCoeffcients::set -> attempt to add existing coeffcient, will override");
  }

  _coeffcients[id] = c;

}

Coeffcient::Ptr Coeffcients::get(Alpha id) {

  Coeffcient::Ptr r;

  if (_coeffcients.count(id) == 0) {
    ofLogNotice("ofxWLCoeffcients::set -> attempt to get missing coeffcient, will return null");
    r = nullptr;
  } else {
    r = _coeffcients[id];
  }

  return r;

}

float Coeffcients::value(Alpha id) {

  float r = 0.0f;
  if (_coeffcients.count(id) > 0) {
    r = _coeffcients[id]->value();
  }
  return r;

}

float Coeffcients::dValue(Alpha id) {

  float r = 0.0f;
  if (_coeffcients.count(id) > 0) {
    r = _coeffcients[id]->dValue();
  }
  return r;

}

Coeffcients::Values& Coeffcients::values() {
  return _values;
}

Coeffcients::Values& Coeffcients::update(float dT) {

  for (_Coeffcients::iterator it = _coeffcients.begin(); it != _coeffcients.end(); ++it) {
    // cout << "update " << it->first << endl;
    _values[it->first] = it->second->update(dT);
  }

  return _values;

}


void Coeffcients::autoUpdate(ofEventArgs& args) {

  //update everything with the correct dT value (i.e. per frame, or since last frame)
  update((_base == FRAME_COUNT) ? _frame_dT : _dT->get());

}