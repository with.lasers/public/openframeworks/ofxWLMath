/*

  ofxWLOrbit - add on for defining orbiting objects which can be used in other functions

  WithLasers::Greek - Constants for greek alphabet


*/

#pragma once

//Open Frameworks Includes


//Add on Includes



//Local Includes

namespace ofxWithLasers {

  namespace Math {

    enum Alpha {
      ALPHA,
      BETA,
      GAMMA,
      DELTA,
      EPSILON,
      ZETA,
      ETA,
      THETA,
      IOTA,
      KAPPA,
      LAMBDA,
      MU,
      NU,
      XI,
      OMICRON,
      PII,
      RHO,
      SIGMA,
      TAU,
      UPSILON,
      PHI,
      CHI,
      PSI,
      OMEGA
    };

  }


}