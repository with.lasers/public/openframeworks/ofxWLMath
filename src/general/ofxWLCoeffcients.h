/*
  ofxWLCoeffcients - ways pf storing calculation coeffcients, and modifying their values
  2021/2022 with-lasers.studio

  TODO:

  User interface options and bindings?

*/

#pragma once

//Openframeworks includes
#include "ofMain.h"

//Addon includes

//Local program includes
#include "ofxWLBounds.h"
#include "ofxWLGreek.h"
#include "ofxWLDeltaT.h"
#include "ofxWLEasings.h"

namespace ofxWithLasers {

  namespace Math {

    class Coeffcient {

      public:

        typedef shared_ptr<Coeffcient> Ptr;

        //Linear conefficents
        Coeffcient(float v, float dV);
        Coeffcient(float v, float dV, Limits::Bounds b, Limits::OutOfBounds a);

        //Recursive Coeffcients
        Coeffcient(float v, Ptr dV);
        Coeffcient(float v, Ptr dV, Limits::Bounds b, Limits::OutOfBounds a);

        //Eased Coeffcients
        Coeffcient(float n, float v, float dV, Limits::OutOfBounds a, Easings::Easing e);

        void setValue(float v);

        void setDelta(float dV);
        void setDelta(Ptr dV);

        void setBounds(Limits::Bounds b);
        void setBounds(Limits::Bounds b, Limits::OutOfBounds a);

        float update(float dT);

        float value();
        float dValue();

      private:

        enum _Kind {
          LINEAR,
          RECURSIVE,
          EASED
        };

        _Kind _kind;

        float _n;                                   //Normalised value for eased coeffcients
        float _v;                                   //Actual value
        float _dV;                                  //Delta value in change per ms / us;

        float  _s;                                  //Scale for eased coeffcients

        Ptr _dVC;

        Easings::Easing _easing;

        Limits::Bounds _bounds;        
        Limits::OutOfBounds _action;

        float _updateLinear(float dT);
        float _updateRecursive(float dT);
        float _updateEased(float dT);

    };


    class Coeffcients {

      public:

        typedef shared_ptr<Coeffcients> Ptr;

        enum TimeBase {
          FRAME_COUNT,
          FRAME_DELTA_T
        };

        Coeffcients(bool autoUpdate = true, TimeBase base = FRAME_DELTA_T, Timing::Resolution res = Timing::MILLI, float frameRate = 60.0f);

        void set(Alpha id, Coeffcient::Ptr c);

        typedef map<Alpha, float> Values;

        Coeffcient::Ptr get(Alpha id);
        float value(Alpha id);
        float dValue(Alpha id);

        Values& values();

        Values& update(float dT);

        void autoUpdate(ofEventArgs& args);

      private:

        typedef map<Alpha, Coeffcient::Ptr> _Coeffcients;
        _Coeffcients _coeffcients;

        TimeBase _base;

        float _frame_dT;
        Timing::DeltaT::Ptr _dT;
        
        Values _values;

    };


  }


}

