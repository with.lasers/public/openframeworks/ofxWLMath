# ofxWLMaths

A collection of math utilities for openframeworks - as of 2022 this is under development, and is an imperfect collection of things, mainly based on things I currently need in my own work.


## Introduction

Useful math functions - as of 2022 these include

### Geometry

`ofxWithLasers::Maths::Spherical` - Spherical(Polar) and cartesian coordinate management

`ofxWithLasers::Maths::Line` - definition of a line as two vertices
`ofxWithLasers::Maths::angleBetweenLines` - angle between two lines
`ofxWithLasers::Maths::distanceToLine` - distance to a line from a vertex

### Limits

`ofxWithLasers::Maths::Bounds(2/3)` - bounds (min/max) in 1,2 and 3 dimensions
`ofxWithLasers::Maths::testBounds(2/3)` - test a point with the bounds, and return if inside it, and expand bounds

### Timings


License
-------
State which license you offer your addon under. openFrameworks is distributed under the [MIT License](https://en.wikipedia.org/wiki/MIT_License), and you might consider using this for your repository. By default, `license.md` contains a copy of the MIT license to which you can add your name and the year.

Installation
------------
Any steps necessary to install your addon. Optimally, this means just dropping the folder into the `openFrameworks/addons/` folder.

Dependencies
------------
What other addons are needed to properly work with this one?

Compatibility
------------
Which versions of OF does this addon work with?

Known issues
------------
Any specific (and long-living) issues/limitations you want to mention? For bugs, etc. you should use the issue tracker of your addon's repository

Version history
------------
It make sense to include a version history here (newest releases first), describing new features and changes to the addon. Use [git tags](http://learn.github.com/p/tagging.html) to mark release points in your repo, too!

### Version 0.1 (Date):
Describe relevant changes etc.


